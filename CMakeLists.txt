cmake_minimum_required( VERSION 3.5 )

find_package( VTK REQUIRED )
include( ${VTK_USE_FILE} )

add_library( chartpp STATIC src/chart_xy.cpp )
add_library( chartpp::chartpp ALIAS chartpp )
target_include_directories( chartpp PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include/chartpp>
)
target_link_libraries( chartpp ${VTK_LIBRARIES} )
set_target_properties( chartpp PROPERTIES
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON
    POSITION_INDEPENDENT_CODE True
)
install( TARGETS chartpp EXPORT chartpp
    ARCHIVE DESTINATION lib
)
install( EXPORT chartpp
    FILE chartpp_targets.cmake
    NAMESPACE chartpp::
    DESTINATION lib/cmake/chartpp
)
install( FILES chartpp-config.cmake DESTINATION lib/cmake/chartpp )
install( DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include/ DESTINATION include/chartpp )

