#include "chart_xy.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <iostream>
#include <limits>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include <vtkAxis.h>
#include <vtkChartHistogram2D.h>
#include <vtkChartXY.h>
#include <vtkContextScene.h>
#include <vtkContextView.h>
#include <vtkSmartPointer.h>
#include <vtkDoubleArray.h>
#include <vtkPlot.h>
#include <vtkPlotHistogram2D.h>
#include <vtkPNGWriter.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkTable.h>
#include <vtkTextProperty.h>
#include <vtkType.h>
#include <vtkWindowToImageFilter.h>

namespace chartpp {

    struct chart_xy::Impl {
        Impl():
            title("Untitled"),
            xlabel("x"),
            ylabel("y"),
            legend{},
            width_in_pixels(560),
            height_in_pixels(420)
        { }

        ~Impl() = default;

        Impl(const Impl&) = default;
        Impl& operator=(const Impl&) = default;

        Impl(Impl&&) = default;
        Impl& operator=(Impl&&) = default;

        std::string title;
        std::string xlabel;
        std::string ylabel;
        std::vector<std::string> legend;
        int width_in_pixels;
        int height_in_pixels;
    };

    chart_xy::chart_xy(): pImpl(std::make_unique<Impl>()) { };
    chart_xy::~chart_xy() = default;

    /*
    void chart_xy::plot(std::string file_name, std::vector<double>& x, std::vector<double>& y) {
        std::vector<XY> lines { XY{x.data(), y.data(), x.size()} };
        plot(std::move(file_name), lines);
    }
    */

    void chart_xy::title(std::string title_in) { pImpl->title = std::move(title_in); }
    std::string chart_xy::title(void) { return pImpl->title; }

    void chart_xy::xlabel(std::string xlabel_in) { pImpl->xlabel = std::move(xlabel_in); }
    std::string chart_xy::xlabel(void) { return pImpl->xlabel; }

    void chart_xy::ylabel(std::string ylabel_in) { pImpl->ylabel = std::move(ylabel_in); }
    std::string chart_xy::ylabel(void) { return pImpl->ylabel; }

    void chart_xy::width_in_pixels(int width_in_pixels_in) {
        if (width_in_pixels_in <= 0) {
            std::ostringstream ss;
            ss << "chart_xy::width_in_pixels takes a non-negative integer, ";
            ss << width_in_pixels_in << " provided.";
            throw std::domain_error(ss.str());
        } else {
            pImpl->width_in_pixels = width_in_pixels_in;
        }
    }
    int chart_xy::width_in_pixels(void) { return pImpl->width_in_pixels; }

    void chart_xy::height_in_pixels(int height_in_pixels_in) {
        if (height_in_pixels_in <= 0) {
            std::ostringstream ss;
            ss << "chart_xy::height_in_pixels takes a non-negative integer, ";
            ss << height_in_pixels_in << " provided.";
            throw std::domain_error(ss.str());
        } else {
            pImpl->height_in_pixels = height_in_pixels_in;
        }
    }
    int chart_xy::height_in_pixels(void) { return pImpl->height_in_pixels; }

    void chart_xy::no_legend(void) { pImpl->legend.clear(); }
    void chart_xy::legend(std::vector<std::string>& legend_in) { pImpl->legend = legend_in; }
    void chart_xy::legend(std::vector<std::string>&& legend_in) { pImpl->legend = std::move(legend_in); }
    std::vector<std::string> chart_xy::legend(void) { return pImpl->legend; }

    void setup_context_view(vtkContextView *view, int width_in_pixels, int height_in_pixels) {
        auto render_window = view->GetRenderWindow();
        render_window->OffScreenRenderingOn();
        render_window->SetSize(width_in_pixels, height_in_pixels);

        view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);  // Set background to white.
    }

    void set_chart_title(vtkChartXY *chart, const char *title, int title_font_size_in_pt) {
        auto title_properties_ptr = chart->GetTitleProperties();

        chart->SetTitle(title);
        
        title_properties_ptr->SetFontFamilyToTimes();
        title_properties_ptr->SetFontSize(title_font_size_in_pt);
        title_properties_ptr->BoldOn();
    }

    void set_chart_title(vtkChartXY *chart, const char *title, int width_in_pixels, int height_in_pixels) {
        auto title_font_size_in_pt = std::min(width_in_pixels/30, height_in_pixels/23);
        set_chart_title(chart, title, title_font_size_in_pt);
    }

    void set_axis_label(vtkAxis *axis, const char *label, int title_font_size_in_pt) {
        auto label_properties_ptr = axis->GetTitleProperties();

        axis->SetTitle(label);

        label_properties_ptr->SetFontFamilyToTimes();
        label_properties_ptr->SetFontSize(title_font_size_in_pt);
        label_properties_ptr->BoldOff();
    }

    void set_axis_label(vtkAxis *axis, const char *label, int width_in_pixels, int height_in_pixels) {
        auto title_font_size_in_pt = std::min(width_in_pixels/30, height_in_pixels/23);
        set_axis_label(axis, label, title_font_size_in_pt);
    }

    void setup_legend_in_chart(const std::vector<std::string>& legend, vtkChartXY *chart) {
        chart->SetShowLegend(!legend.empty());
    }

    void apply_legend_to_y_column(std::vector<std::string>& legend, vtkDoubleArray *y_column, std::size_t xy_pair_index) {
        if (xy_pair_index < legend.size()) {
            y_column->SetName(legend[xy_pair_index].c_str());
        } else {
            y_column->SetName("untitled");
        }
    }

    struct Color { unsigned char red; unsigned char green; unsigned char blue; };

    void add_single_plot_to_chart(
        vtkChartXY *chart,
        int plot_type,
        const Color& color,
        vtkTable *table_xy
    ) {
        auto *plot = chart->AddPlot(plot_type);
        plot->SetInputData(table_xy, 0, 1);     // x is column 0, y is column 1
        plot->SetWidth(1.0);
        plot->SetColor(color.red, color.green, color.blue, 255);    // Alpha is 255, fully opaque.
    }

    template <typename T>
    void add_single_plot_to_chart(
        vtkChartXY *chart,
        int plot_type,
        const Color& color,
        std::vector<std::string>& legend,
        const T& xy_pair_to_plot,   // T should be XY, but since XY is private to chart_xy, it needs to be a template.
        std::size_t xy_pair_index,
        std::vector<vtkSmartPointer<vtkTable>>& table_storage,  // All columns of a table must have the same size, use different table of each XY.
        std::vector<vtkSmartPointer<vtkDoubleArray>>& column_storage
    ) {
        auto table = vtkSmartPointer<vtkTable>::New();
        auto x_column = vtkSmartPointer<vtkDoubleArray>::New();
        auto y_column = vtkSmartPointer<vtkDoubleArray>::New();

        table_storage.push_back(table);
        column_storage.push_back(x_column);
        column_storage.push_back(y_column);

        table->AddColumn(x_column);
        table->AddColumn(y_column);

        constexpr int do_not_delete_array = 1;
        auto size = static_cast<vtkIdType>(xy_pair_to_plot.size);

        // Justify the const_casts by testing the const assumption with unit tests.

        x_column->SetArray(const_cast<double*>(xy_pair_to_plot.x), size, do_not_delete_array);
        x_column->SetName("x");

        y_column->SetArray(const_cast<double*>(xy_pair_to_plot.y), size, do_not_delete_array);
        apply_legend_to_y_column(legend, y_column, xy_pair_index);

        add_single_plot_to_chart(chart, plot_type, color, table);
    }

    template <typename T>
    void add_plots_to_chart(
        vtkChartXY *chart,
        int plot_type,
        const std::vector<Color>& colors,
        std::vector<std::string>& legend,
        const T& data_to_plot,  // T should be std::vector<XY>, but since XY is private to chart_xy, it needs to be a template.
        std::vector<vtkSmartPointer<vtkTable>>& table_storage,
        std::vector<vtkSmartPointer<vtkDoubleArray>>& column_storage
    ) {
        for (std::size_t i = 0; i != data_to_plot.size(); ++i) {
            auto color = i<colors.size() ? colors[i] : Color{0,0,0};
            add_single_plot_to_chart(chart, plot_type, color, legend, data_to_plot[i], i, table_storage, column_storage);
        }
    }

    void render_and_save(const char *file_name, vtkContextView *view) {
        auto *render_window = view->GetRenderWindow();

        render_window->Render();

        auto windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();
        windowToImageFilter->SetInput(render_window);
        windowToImageFilter->Update();

        auto writer = vtkSmartPointer<vtkPNGWriter>::New();
        writer->SetFileName(file_name);
        writer->SetInputConnection(windowToImageFilter->GetOutputPort());

        writer->Write();
    }

    template <class T1, class T2>
    void lines_chart_base(int chart_type, T1& pImpl, const std::string& file_name_without_extension, const T2& lines) {
        // Use the same default colors as in matlab.
        static const std::vector<Color> default_colors = {
            Color{0, 114, 189},
            Color{217, 83, 25},
            Color{237, 177, 32},
            Color{126, 47, 142},
            Color{119, 172, 48},
            Color{77, 190, 238},
            Color{162, 20, 47}
        };

        auto file_name_with_extension = file_name_without_extension + ".png";

        auto width_in_pixels = pImpl->width_in_pixels;
        auto height_in_pixels = pImpl->height_in_pixels;
        auto& legend = pImpl->legend;

        auto view = vtkSmartPointer<vtkContextView>::New();
        setup_context_view(view, width_in_pixels, height_in_pixels);

        auto chart = vtkSmartPointer<vtkChartXY>::New();
        view->GetScene()->AddItem(chart);
        setup_legend_in_chart(legend, chart);
        set_chart_title(chart, pImpl->title.c_str(), width_in_pixels, height_in_pixels);
        set_axis_label(chart->GetAxis(0), pImpl->ylabel.c_str(), width_in_pixels, height_in_pixels);
        set_axis_label(chart->GetAxis(1), pImpl->xlabel.c_str(), width_in_pixels, height_in_pixels);

        std::vector<vtkSmartPointer<vtkTable>> table_storage;
        std::vector<vtkSmartPointer<vtkDoubleArray>> column_storage;

        add_plots_to_chart(chart, chart_type, default_colors, legend, lines, table_storage, column_storage);

        render_and_save(file_name_with_extension.c_str(), view);
    }

    void chart_xy::plot(const std::string& file_name_without_extension, const std::vector<XY>& lines) {
        lines_chart_base(vtkChart::LINE, pImpl, file_name_without_extension, lines);
    }

    // Think about how to handle infinities and NaNs in histogram and plot for public version.

    double histogram_lower_bound(const double *sorted_observations, std::size_t size) {
        return std::nexttoward(sorted_observations[0], std::numeric_limits<double>::lowest());
    }

    double histogram_upper_bound(const double *sorted_observations, std::size_t size) {
        return std::nexttoward(sorted_observations[size-1], std::numeric_limits<double>::max());
    }

    double histogram_bin_width(double lower_bound, double upper_bound, long long number_of_bins) {
        return (upper_bound - lower_bound)/number_of_bins;
    }

    double histogram_frequency_mean(const double *sorted_observations, std::size_t size, double lower_bound, double bin_width, long long number_of_bins) {
        // See https://en.wikipedia.org/wiki/Kahan_summation_algorithm

        auto sum = 0.0;
        auto compensation = 0.0;
        auto data_index = static_cast<std::size_t>(0);
        for (long long bin_number = 1; data_index != size && bin_number <= number_of_bins; ++bin_number) {
            long long frequency = 0;
            for (; data_index != size && sorted_observations[data_index] < (lower_bound + bin_number*bin_width); ++data_index) {
                ++frequency;
            }
            auto y = static_cast<double>(frequency) - compensation;
            auto t = sum + y;
            compensation = (t - sum) - y;
            sum = t;
        }
        return sum/number_of_bins;
    }

    double histogram_frequency_biased_variance(const double *sorted_observations, std::size_t size, double lower_bound, double bin_width, long long number_of_bins, double mean_frequency) {
        // See https://en.wikipedia.org/wiki/Kahan_summation_algorithm

        auto sum = 0.0;
        auto compensation = 0.0;
        auto data_index = static_cast<std::size_t>(0);
        for (long long bin_number = 1; data_index != size && bin_number <= number_of_bins; ++bin_number) {
            auto frequency = static_cast<long long>(0);
            for (; data_index != size && sorted_observations[data_index] < (lower_bound + bin_number*bin_width); ++data_index) {
                ++frequency;
            }
            auto error = static_cast<double>(frequency - mean_frequency);
            auto error2 = error*error;
            auto y = error2 - compensation;
            auto t = sum + y;
            compensation = (t - sum) - y;
            sum = t;
        }
        return sum/number_of_bins;
    }

    double histogram_bin_width_cost(long long frequency_mean, long long frequency_variance, double bin_width) {
        return (2*frequency_mean - frequency_variance)/(bin_width*bin_width);
    }

    long long histogram_number_of_bins(const double *sorted_observations, std::size_t size, double hist_lower_bound, double hist_upper_bound) {
        // See below for method for determining bin width.
        // http://176.32.89.45/~hideaki/res/histogram.html

        auto bins_upper_bound = std::min(size, static_cast<size_t>(100));

        auto min_cost = std::numeric_limits<double>::max();
        auto number_of_bins_min_cost = static_cast<long long>(1);
        for (std::size_t number_of_bins = 1 ; number_of_bins <= bins_upper_bound; ++number_of_bins) {
            auto bin_width = histogram_bin_width(hist_lower_bound, hist_upper_bound, number_of_bins);
            auto frequency_mean = histogram_frequency_mean(sorted_observations, size, hist_lower_bound, bin_width, number_of_bins);
            auto frequency_variance = histogram_frequency_biased_variance(sorted_observations, size, hist_lower_bound, bin_width, number_of_bins, frequency_mean);
            auto cost = histogram_bin_width_cost(frequency_mean, frequency_variance, bin_width);
            if (cost < min_cost) {
                min_cost = cost;
                number_of_bins_min_cost = number_of_bins;
            }
        }

        return number_of_bins_min_cost;
    }

    std::vector<double> get_histogram_frequencies(const double *sorted_observations, std::size_t size, double lower_bound, double bin_width, long long number_of_bins) {
        std::vector<double> frequencies(number_of_bins);
        auto sum_of_frequencies = static_cast<long long>(0);
        auto data_index = static_cast<std::size_t>(0);
        for (long long bin_number = 1; data_index != size && bin_number <= number_of_bins; ++bin_number) {
            auto frequency = static_cast<long long>(0);
            for (; data_index != size && sorted_observations[data_index] < (lower_bound + bin_number*bin_width); ++data_index) {
                ++frequency;
            }
            frequencies[bin_number-1] = frequency/(bin_width*size); // Normalise frequencies so that the area integrates to one.
        }
        return frequencies;
    }

    struct HistogramOutline {
        std::vector<double> x;
        std::vector<double> y;
    };

    HistogramOutline get_histogram_outline(double lower_bound, double range, const std::vector<double> frequencies) {
        auto number_of_bins = static_cast<long long>(frequencies.size());

        HistogramOutline outline;
        auto& x = outline.x;
        auto& y = outline.y;

        auto histogram_outline_size = static_cast<std::size_t>(2*(number_of_bins + 1));
        x.reserve(histogram_outline_size);
        y.reserve(histogram_outline_size);

        auto get_boundary = [lower_bound, range, number_of_bins](long long boundary_number) {
            return lower_bound + (static_cast<double>(boundary_number)/number_of_bins)*range;
        };

        x.push_back(lower_bound);
        y.push_back(0.0);
        for (long long boundary_number = 0; boundary_number != number_of_bins; ++boundary_number) {
            auto frequency = frequencies[boundary_number];

            x.push_back(get_boundary(boundary_number));
            y.push_back(frequency);

            x.push_back(get_boundary(boundary_number + 1));
            y.push_back(frequency);
        }
        x.push_back(get_boundary(number_of_bins));
        y.push_back(0.0);

        return outline;
    }

    struct BoundaryOutline {
        std::array<double, 2> x;
        std::array<double, 2> y;
    };

    std::vector<BoundaryOutline> get_boundary_outlines(double lower_bound, double range, const std::vector<double> frequencies) {
        auto number_of_bins = static_cast<long long>(frequencies.size());

        std::vector<BoundaryOutline> outlines;
        outlines.reserve(number_of_bins - 1);

        auto get_boundary = [lower_bound, range, number_of_bins](long long boundary_number) {
            return lower_bound + (static_cast<double>(boundary_number)/number_of_bins)*range;
        };

        for (long long boundary_number = 1; boundary_number != number_of_bins; ++boundary_number) {
            double boundary = get_boundary(boundary_number);
            
            outlines.emplace_back();

            BoundaryOutline& boundary_outline = outlines.back();
            std::array<double, 2>& x = boundary_outline.x;
            std::array<double, 2>& y = boundary_outline.y;

            x[0] = boundary;
            y[0] = 0.0;

            x[1] = boundary;
            y[1] = std::min(frequencies[boundary_number-1], frequencies[boundary_number]);
        }

        return outlines;
    }

            

    void chart_xy::histogram_sorted(const std::string& file_name_without_extension, const double *sorted_observations, std::size_t size) {
        if (size < 3) throw std::invalid_argument("chart_xy::histogram and chart_xy::histogram_sorted require at least three observations.");

        auto hist_lower_bound = histogram_lower_bound(sorted_observations, size);
        auto hist_upper_bound = histogram_upper_bound(sorted_observations, size);
        auto hist_range = hist_upper_bound - hist_lower_bound;

        auto number_of_bins = histogram_number_of_bins(sorted_observations, size, hist_lower_bound, hist_upper_bound);
        auto bin_width = histogram_bin_width(hist_lower_bound, hist_upper_bound, number_of_bins);
        
        auto frequencies = get_histogram_frequencies(sorted_observations, size, hist_lower_bound, bin_width, number_of_bins);
        
        auto histogram_outline = get_histogram_outline(hist_lower_bound, hist_range, frequencies);
        auto boundary_outlines = get_boundary_outlines(hist_lower_bound, hist_range, frequencies);

        std::vector<XY> histogram_outline_lines = { XY{histogram_outline.x.data(), histogram_outline.y.data(), histogram_outline.x.size()} };

        std::vector<XY> boundary_outlines_lines;
        for (auto iter = boundary_outlines.cbegin(); iter != boundary_outlines.cend(); ++iter) {
            boundary_outlines_lines.emplace_back(iter->x.data(), iter->y.data(), iter->x.size());
        }

        auto file_name_with_extension = file_name_without_extension + ".png";

        auto width_in_pixels = pImpl->width_in_pixels;
        auto height_in_pixels = pImpl->height_in_pixels;

        auto view = vtkSmartPointer<vtkContextView>::New();
        setup_context_view(view, width_in_pixels, height_in_pixels);

        auto chart = vtkSmartPointer<vtkChartXY>::New();
        view->GetScene()->AddItem(chart);
        set_chart_title(chart, pImpl->title.c_str(), width_in_pixels, height_in_pixels);
        set_axis_label(chart->GetAxis(0), pImpl->ylabel.c_str(), width_in_pixels, height_in_pixels);
        set_axis_label(chart->GetAxis(1), pImpl->xlabel.c_str(), width_in_pixels, height_in_pixels);

        std::vector<vtkSmartPointer<vtkTable>> table_storage;
        std::vector<vtkSmartPointer<vtkDoubleArray>> column_storage;

        std::vector<Color> no_color;
        std::vector<std::string> no_legend;

        //add_plots_to_chart(chart, vtkChart::LINE, no_color, no_legend, histogram_outline_lines, table_storage, column_storage);
        add_plots_to_chart(chart, vtkChart::STACKED, std::vector<Color>{{ Color{0, 114, 189} }}, no_legend, histogram_outline_lines, table_storage, column_storage);
        add_plots_to_chart(chart, vtkChart::LINE, no_color, no_legend, histogram_outline_lines, table_storage, column_storage);
        add_plots_to_chart(chart ,vtkChart::LINE, no_color, no_legend, boundary_outlines_lines, table_storage, column_storage);

        render_and_save(file_name_with_extension.c_str(), view);
    }

}

