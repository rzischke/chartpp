#ifndef CHARTXY_GUARD
#define CHARTXY_GUARD

#include <algorithm>
#include <array>
#include <functional>
#include <memory>
#include <stdexcept>
#include <string>
#include <sstream>
#include <type_traits>
#include <vector>

namespace chartpp{

    class chart_xy {
        public:
            chart_xy();
            ~chart_xy();

            chart_xy(const chart_xy&) = default;
            chart_xy& operator=(const chart_xy&) = default;

            chart_xy(chart_xy&&) = default;
            chart_xy& operator=(chart_xy&&) = default;

            void title(std::string);
            std::string title(void);

            void xlabel(std::string);
            std::string xlabel(void);

            void ylabel(std::string);
            std::string ylabel(void);

            void width_in_pixels(int);
            int width_in_pixels(void);

            void height_in_pixels(int);
            int height_in_pixels(void);

            void no_legend(void);
            void legend(std::vector<std::string>&);
            void legend(std::vector<std::string>&&);
            std::vector<std::string> legend(void);

            template <class... Strings>
            void legend(Strings&&... legend_in) {
                std::vector<std::string> legend_vec;
                legend(legend_vec, std::forward<Strings>(legend_in)...);
                legend(std::move(legend_vec));
            }

            template <class Container, class... Containers>
            void plot(const std::string& file_name, const Container& first, const Containers&... the_rest) {
                std::vector<XY> lines;
                plot_at_an_x(file_name, lines, first, the_rest...);
            }

            template <class Container>
            void plot(const std::string& file_name, const Container& y) {
                std::size_t sz = y.size();
                std::vector<double> x(sz);
                for (std::size_t i = 0; i != sz; ++i) {
                    x[i] = i;
                }
                plot(file_name, x, y);
            }

            void histogram_sorted(const std::string& file_name, const std::vector<double>& sorted_observations) {
                histogram_sorted(file_name, sorted_observations.data(), sorted_observations.size());
            }

            template <std::size_t N>
            void histogram_sorted(const std::string& file_name, const std::array<double, N>& sorted_observations) {
                histogram_sorted(file_name, sorted_observations.data(), sorted_observations.size());
            }

            template <class Container>
            void histogram(const std::string& file_name, const Container& observations) {
                std::vector<double> observations_vec(observations.cbegin(), observations.cend());
                std::sort(observations_vec.begin(), observations_vec.end());
                histogram_sorted(file_name, observations_vec);
            }
            
            // void plot(std::string file_name, std::vector<double>& x, std::vector<double>& y);

            /* Do after MVP.
            template<long N = 1l, class T, class... Ts>     // Specify that these must be strings so that the iterators can resolve to below definition.
            void legend(T next_entry, Ts... the_rest) {
                // add the next_entry 
                legend<N+1l>(std::move(the_rest)...);
            }

            template <std::string>
            void legend(std::string entry1) {

            }

            template <class InputIterator>
            void legend(InputIterator begin, InputIterator end) {

            }

            template <template<typename...> class Container>
            void legend(Container entries) {
                legend(std::begin(entries), std::end(entries));
            }

            template <>
            std::vector<std::string> legend(void);
            */

            // Get the above working first, then move on to controlling axes.

        private:
            struct Impl;
            std::unique_ptr<Impl> pImpl;

            struct XY {
                XY(const double* x_in, const double* y_in, std::size_t size_in): x(x_in), y(y_in), size(size_in) { }

                const double* x;
                const double* y;
                std::size_t size;
                //std::string line_spec; // Have an object for this that parses the string in the constructor.
            };

            void throw_if_size_mismatch(std::size_t x_size, std::size_t y_size) {
                if (x_size != y_size) {
                    std::ostringstream ss;
                    ss << "An (x,y) pair passed to chart_xy::plot have different sizes. "
                          "(x.size() == " << x_size << " && y.size() == " << y_size << ")";
                    throw std::invalid_argument(ss.str());
                }
            }
     
            template <class String>
            void legend(std::vector<std::string>& legend_vec, String&& entry) {
                legend_vec.emplace_back(std::forward<String>(entry));
            }

            template <class String, class... Strings>
            void legend(std::vector<std::string>& legend_vec, String&& entry, Strings&&... the_rest) {
                legend(legend_vec, entry);
                legend(legend_vec, std::forward<Strings>(the_rest)...);
            }

            void histogram_sorted(const std::string&, const double*, std::size_t); // Defined in cpp.

            void plot(const std::string& file_name, const std::vector<XY>& lines);          // Defined in cpp.

            void add_y_data_to_lines(std::vector<XY>& lines, const double *y_data, std::size_t y_size) {
                auto rvs_iter = std::find_if(lines.rbegin(), lines.rend(), [](XY xy) { return xy.y != nullptr;});
                if (rvs_iter == lines.rbegin()) {
                    throw std::invalid_argument("An (X,Y) pair passed to chart_xy::plot had more ys in Y than xs in X.");
                }
                rvs_iter--;
                throw_if_size_mismatch(rvs_iter->size, y_size);
                rvs_iter->y = y_data;
            }

            void validate_complete_lines(std::vector<XY>& lines) {
                if (!lines.empty() && lines.back().y == nullptr) throw std::invalid_argument("An (X,Y) pair passed to chart_xy::plot had more xs in X than yx in Y.");
            }

            void plot_at_a_y(
                const std::string& file_name,
                std::vector<XY>& lines,
                const double* y_data,
                std::size_t y_size
            ) {
                add_y_data_to_lines(lines, y_data, y_size);
                validate_complete_lines(lines);
                plot(file_name, lines);
            }

            void plot_at_a_y(
                const std::string& file_name,
                std::vector<XY>& lines,
                const std::vector<double>& y_data
            ) {
                plot_at_a_y(file_name, lines, y_data.data(), y_data.size());
            }

            template <std::size_t N>
            void plot_at_a_y(
                const std::string& file_name,
                std::vector<XY>& lines,
                const std::array<double, N>& y_data
            ) {
                plot_at_a_y(file_name, lines, y_data.data(), y_data.size());
            }

            template <class YContainer>
            void plot_at_a_y(
                const std::string& file_name,
                std::vector<XY>& lines,
                const YContainer& y_data
            ) {
                std::vector<double> y_data_vector(y_data.begin(), y_data.end());
                plot_at_a_y(file_name, lines, y_data_vector);
            }

            template <class... Containers>
            void plot_at_a_y(
                const std::string& file_name,
                std::vector<XY>& lines,
                const double* y_data,
                std::size_t y_size,
                const Containers&... the_rest
            ) {
                add_y_data_to_lines(lines, y_data, y_size);
                plot_at_an_x(file_name, lines, the_rest...);
            }

            template <class... Containers>
            void plot_at_a_y(
                const std::string& file_name,
                std::vector<XY>& lines,
                const std::vector<double>& y_data,
                const Containers&... the_rest
            ) {
                plot_at_a_y(file_name, lines, y_data.data(), y_data.size(), the_rest...);
            }

            template <std::size_t N, class... Containers>
            void plot_at_a_y(
                const std::string& file_name,
                std::vector<XY>& lines,
                const std::array<double, N>& y_data,
                const Containers&... the_rest
            ) {
                plot_at_a_y(file_name, lines, y_data.data(), y_data.size(), the_rest...);
            }

            template <class YContainer, class... Containers>
            void plot_at_a_y(
                const std::string& file_name,
                std::vector<XY>& lines,
                const YContainer& y_data,
                const Containers&... the_rest
            ) {
                std::vector<double> y_data_vector(y_data.begin(), y_data.end());
                plot_at_a_y(file_name, lines, y_data_vector, the_rest...);
            }

            template <template<class...> class YContainer>
            void plot_at_a_y(
                const std::string& file_name,
                std::vector<XY>& lines,
                const YContainer<std::vector<double>> Y_data
            ) {
                for (auto iter = Y_data.begin(); iter != Y_data.end(); ++iter) {
                    add_y_data_to_lines(lines, iter->data(), iter->size());
                }
                validate_complete_lines(lines);
                plot(file_name, lines);
            }

            template <template<class...> class YContainer, class... Containers>
            void plot_at_a_y(
                const std::string& file_name,
                std::vector<XY>& lines,
                const YContainer<std::vector<double>> Y_data,
                const Containers&... the_rest
            ) {
                for (auto iter = Y_data.begin(); iter != Y_data.end(); ++iter) {
                    add_y_data_to_lines(lines, iter->data(), iter->size());
                }
                plot_at_an_x(file_name, lines, the_rest...);
            }

            template <template<class...> class YContainer>
            void plot_at_a_y(
                const std::string& file_name,
                std::vector<XY>& lines,
                const YContainer<std::reference_wrapper<std::vector<double>>> Y_data_refs
            ) {
                for (auto iter = Y_data_refs.begin(); iter != Y_data_refs.end(); ++iter) {
                    const auto& y_data = iter->get();
                    add_y_data_to_lines(lines, y_data.data(), y_data.size());
                }
                validate_complete_lines(lines);
                plot(file_name, lines);
            }

            template <template<class...> class YContainer, class... Containers>
            void plot_at_a_y(
                const std::string& file_name,
                std::vector<XY>& lines,
                const YContainer<std::reference_wrapper<std::vector<double>>> Y_data_refs,
                const Containers&... the_rest
            ) {
                for (auto iter = Y_data_refs.begin(); iter != Y_data_refs.end(); ++iter) {
                    const auto& y_data = iter->get();
                    add_y_data_to_lines(lines, y_data.data(), y_data.size());
                }
                plot_at_an_x(file_name, lines, the_rest...);
            }

            void add_x_data_to_lines(std::vector<XY>& lines, const double* x_data, std::size_t x_size) {
                XY line {x_data, nullptr, x_size};
                lines.emplace_back(std::move(line));
            }

            template <class XContainer>
            void plot_at_an_x(const std::string&, std::vector<XY>&, const XContainer&) {
                struct hidden_type { };
                static_assert(
                    std::is_same<XContainer, hidden_type>::value,  // False if templated is instantiated.
                    "An odd number of arguments was passed to chart_xy::plot."
                );
            }

            template <class... Containers>
            void plot_at_an_x(
                const std::string& file_name,
                std::vector<XY>& lines,
                const double* x_data,
                std::size_t x_size,
                const Containers&... the_rest
            ) {
                add_x_data_to_lines(lines, x_data, x_size);
                plot_at_a_y(file_name, lines, the_rest...);
            }

            template <class... Containers>
            void plot_at_an_x(
                const std::string& file_name,
                std::vector<XY>& lines,
                const std::vector<double>& x_data,
                const Containers&... the_rest
            ) {
                plot_at_an_x(file_name, lines, x_data.data(), x_data.size(), the_rest...);
            }

            template <std::size_t N, class... Containers>
            void plot_at_an_x(
                const std::string& file_name,
                std::vector<XY>& lines,
                const std::array<double, N>& x_data,
                const Containers&... the_rest
            ) {
                plot_at_an_x(file_name, lines, x_data.data(), x_data.size(), the_rest...);
            }

            template <class XContainer, class... Containers>
            void plot_at_an_x(
                const std::string& file_name,
                std::vector<XY>& lines,
                const XContainer& x_data,
                const Containers&... the_rest
            ) {
                std::vector<double> x_data_vector(x_data.begin(), x_data.end());    // Later on maybe change this to std::begin(.) and end().
                plot_at_an_x(file_name, lines, x_data_vector, the_rest...);
            }

            template <template<class...> class XContainer, class... Containers>
            void plot_at_an_x(
                const std::string& file_name,
                std::vector<XY>& lines,
                const XContainer<std::vector<double>>& X_data,
                const Containers&... the_rest
            ) {
                for (auto iter = X_data.begin(); iter != X_data.end(); ++iter) {
                    add_x_data_to_lines(lines, iter->data(), iter->size());
                }
                plot_at_a_y(file_name, lines, the_rest...);
            }

            template <template<class...> class XContainer, class... Containers>
            void plot_at_an_x(
                const std::string& file_name,
                std::vector<XY>& lines,
                const XContainer<std::reference_wrapper<std::vector<double>>> X_data_refs,
                const Containers&... the_rest
            ) {
                for (auto iter = X_data_refs.begin(); iter != X_data_refs.end(); ++iter) {
                    const auto& x_data = iter->get();
                    add_x_data_to_lines(lines, x_data.data(), x_data.size());
                }
                plot_at_a_y(file_name, lines, the_rest...);
            }

            // Now do efficient versions c arrays.

            //void save_as_bmp(const char *);           // These belong in impl.
            //void save_as_bmp(const std::string&);

            //void save_as_jpeg(const char *);
            //void save_as_jpeg(const std::string&);

            //void save_as_png(const char *);
            //void save_as_png(const std::string&);

            //void save_as_tiff(const char *);
            //void save_as_tiff(const std::string&);

            //void save_as(const char *);         // Determine the format from the extension after the final dot.
            //void save_as(const std::string&);   // Set file name in constructor, write to file in destructor.

    };

}

#endif
