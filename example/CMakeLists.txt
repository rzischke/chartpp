cmake_minimum_required( VERSION 3.5 )

find_package( chartpp )

add_executable( trig trig.cpp )
target_link_libraries( trig chartpp::chartpp )
set_target_properties( trig PROPERTIES
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON
)

