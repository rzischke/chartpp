#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>
#include <list>
#include <random>
#include <vector>

#include "chart_xy.hpp"

using std::cout;
using std::endl;
using std::array;
using std::list;
using std::vector;

int main() {
    constexpr double PI = 3.141592653589793238463;
    constexpr double PIon4 = PI/4.0;

    constexpr int n1 = 1001;
    constexpr double max = 7.0;
    vector<double> y1(n1), y6(n1), y7(n1);
    array<double, n1> y0, y4, y5;
    list<double> x, y2, y3;

    for (int i = 0; i != n1; ++i) {
        double x_i = (i*max)/n1;
        double y0_i = cos(x_i);
        double y1_i = cos(x_i - PIon4);
        double y2_i = cos(x_i - 2*PIon4);
        double y3_i = cos(x_i - 3*PIon4);
        double y4_i = cos(x_i - 4*PIon4);
        double y5_i = cos(x_i - 5*PIon4);
        double y6_i = cos(x_i - 6*PIon4);
        double y7_i = cos(x_i - 7*PIon4);
        x.push_back(x_i);
        y0[i] = y0_i;
        y1[i] = y1_i;
        y2.push_back(y2_i);
        y3.push_back(y3_i);
        y4[i] = y4_i;
        y5[i] = y5_i;
        y6[i] = y6_i;
        y7[i] = y7_i;
    }

    chartpp::chart_xy trig_plot;
    trig_plot.title("Trigonometry");
    trig_plot.xlabel("x");
    trig_plot.ylabel("y");
    trig_plot.legend(
        "y = cos(x)",
        "y = cos(x + (1/4)pi)",
        "y = cos(x + (1/2)pi)",
        "y = cos(x + (3/4)pi)",
        "y = cos(x + pi)",
        "y = cos(x + (5/4)pi)",
        "y = cos(x + (3/2)pi)",
        "y = cos(x + (7/4)pi)"
    );
    //trig_plot.no_legend();
    trig_plot.width_in_pixels(1920);
    trig_plot.height_in_pixels(1080);
    trig_plot.plot(
        "trig",
        x,y0,
        x,y1,
        x,y2,
        x,y3,
        x,y4,
        x,y5,
        x,y6,
        x,y7
    );

    constexpr int n2 = 1000000;

    std::default_random_engine generator;
    std::normal_distribution<double> distribution;

    std::vector<double> norm_observations;
    std::vector<double> trig_observations;
    for (int i = 0; i != n2; ++i) {
        auto normal_observation = distribution(generator);
        norm_observations.push_back(normal_observation);
        trig_observations.push_back(cos(normal_observation));
    }
    std::sort(norm_observations.begin(), norm_observations.end());
    std::sort(trig_observations.begin(), trig_observations.end());

    chartpp::chart_xy trig_hist;
    trig_hist.title("Histogram of Cos(Z), Z ~ N(0,1)");
    trig_hist.xlabel("Observation");
    trig_hist.ylabel("Normalised Frequency");
    trig_hist.width_in_pixels(1920);
    trig_hist.height_in_pixels(1080);
    trig_hist.histogram_sorted("trig_hist", trig_observations);

    chartpp::chart_xy norm_hist;
    norm_hist.title("Histogram of Z ~ N(0,1)");
    norm_hist.xlabel("Observation");
    norm_hist.ylabel("Normalised Frequency");
    norm_hist.width_in_pixels(1920);
    norm_hist.height_in_pixels(1080);
    norm_hist.histogram_sorted("norm_hist", norm_observations);

    return EXIT_SUCCESS;
}

